import mongoose from "mongoose";
import env from '../Configs/env'
export default async () => {
  try {
    const connection = await mongoose.connect(env.databaseURL, {
      useNewUrlParser: true,
    });
    return connection.connection.db;
  } catch (error) {
    return error;
  }
};
