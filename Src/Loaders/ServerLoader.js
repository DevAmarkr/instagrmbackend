import express from "express";
import serverLog from "morgan";
import mongoLoader from "./MogoLoader";
import {routeGates} from '../Apis'
console.log(routeGates,'routes')
class Network {
  constructor(port) {
    this.app = express();
    this.useMiddleware = express.Router()
    this.express = express;
    this.PORT = port ? port : 2020;
  }
async routes() {
    this.app.use('/',routeGates)

  }
  async listening(dbName) {
    let el = await dbName;
    return this.app.listen(this.PORT, () =>
      console.log(
        `
        SERVER-PORT:${this.PORT} 
        DB-Name:${el["databaseName"]}
        `
      )
    );
  }
  async database() {
    return await mongoLoader();
  }
  serverMiddleware() {
    this.app.use(serverLog("dev"));
    this.app.use(this.express.json());
    // this.app.use((req, res) => {
    //   console.log(req, "REQ");
    // });
    this.app.use((err, req, res, next)=> {
    });
  }

  async startServerAndDB() {
    this.listening(this.database());
    this.routes();
    this.serverMiddleware()
  }
}
module.exports = Network;

