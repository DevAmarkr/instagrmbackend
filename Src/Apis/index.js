import express from 'express'
const router  = express.Router()
import {userRouter} from './User/index'
import {postRouter} from './Post/index'

class NetworkMainRouterHandler{
    constructor(state){
        this.middleware = express()
        this.router = router
    
    }
    api(){
    return[
        this.router.get('/',function(req,res){res.send(`<h1>Instagram App</h1>`)}),
        this.middleware.use('/User',userRouter),
        this.middleware.use('/Post',postRouter)
    ]
}    

}
let apiRouter = new NetworkMainRouterHandler()

module.exports = {routeGates:apiRouter.api()}
